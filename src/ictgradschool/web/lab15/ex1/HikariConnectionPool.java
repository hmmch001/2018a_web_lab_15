package ictgradschool.web.lab15.ex1;

import com.zaxxer.hikari.HikariDataSource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class HikariConnectionPool {
    private static HikariDataSource hds;

    static {

        Properties dbProps = new Properties();
        try (FileInputStream fis = new FileInputStream("connection.properties")) {
            dbProps.load(fis);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        hds = new HikariDataSource();
        hds.setJdbcUrl("jdbc:mysql://db.sporadic.nz:3306/hmch001");
        hds.setDriverClassName("com.mysql.jdbc.Driver");
        hds.setUsername("hmch001");
        hds.setPassword("PlainPrivateVirtueTrap");
        hds.setMaximumPoolSize(2);
    }

    public static Connection getConnection() throws SQLException {
        return hds.getConnection();
    }
}