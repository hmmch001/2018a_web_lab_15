package ictgradschool.web.lab15.ex1;

import java.sql.Timestamp;

/**
 * Created by Andrew Meads on 3/01/2018.
 */
public class AccessLog {

    private Integer id;

    private String name;

    private String description;

    private Timestamp time;

    public AccessLog() {
    }

    public AccessLog(int id, String name, String description, Timestamp time) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}

//    @Override
//    public String toString() {
//        return
//    }
//}
