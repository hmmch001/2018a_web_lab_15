package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class LoggingTableNewEntry extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try(AccessLogDAO dao = new AccessLogDAO()) {
            String name = request.getParameter("Name");
            System.out.println(name);
            String description = request.getParameter("Description");
            System.out.println(description);
            AccessLog log = new AccessLog();
            log.setName(name);
            log.setDescription(description);
            dao.addAccessLog(log);
        }catch(SQLException e){
            System.out.println(e);
        }
        PrintWriter pw = response.getWriter();
        pw.print("<h1>Submitted successfully.</h1>");
        pw.print(("<h1><a href = \"/question1\">Click here to return to table</a></h1>"));


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        PrintWriter pw = response.getWriter();

        pw.print("<h1>405 - GET method not supported</h1>");

        pw.close();
    }
}
