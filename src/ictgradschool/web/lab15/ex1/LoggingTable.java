package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

public class LoggingTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();

        pw.print("<p>Display your table and form here</p>");
        pw.print("<table><tr><td> Name</td><td> Description</td></tr>");
        try(AccessLogDAO dao = new AccessLogDAO()) {
            List<AccessLog> lecturers = dao.getAllAccessLogs();
            for (AccessLog log : lecturers){
                pw.print("<tr><td>" + log.getName() + "</td><td>" + log.getDescription() + "</td></tr>");
            }
        }catch(SQLException e){
            System.out.println(e);
        }
        pw.print("<form action = \"/question1/new\" method = \"POST\">\n"+
                "<label for=\"nameID\">Name:</label>\n" +
                "<br><input type=\"text\" id=\"nameID\" name=\"Name\">\n" +
                "<br><label for=\"descriptionID\">Description:</label>\n" +
                "<br><input type=\"text\" id=\"descriptionID\" name=\"Description\">\n"+
                "<br>\n" +
                "<input type=\"submit\" value=\"Submit\">\n" +
                "</form>");

        pw.close();
    }
}
