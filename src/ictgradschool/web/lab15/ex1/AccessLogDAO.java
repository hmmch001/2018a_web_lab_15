package ictgradschool.web.lab15.ex1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class AccessLogDAO implements AutoCloseable {

    private final Connection conn;


    public AccessLogDAO() throws SQLException {

        this.conn = HikariConnectionPool.getConnection();
    }


    public List<AccessLog> getAllAccessLogs() throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM access_log")) {
            try (ResultSet rs = stmt.executeQuery()) {

                List<AccessLog> log = new ArrayList<>();
                while (rs.next()) {
                    log.add(accessLogFromResultSet(rs));
                }

                return log;

            }
        }

    }


    public AccessLog getAccessLogsById(int id) throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM access_log WHERE id = ?;")) {

            stmt.setInt(1, id);

            try (ResultSet rs = stmt.executeQuery()) {

                if (rs.next()) {
                    return accessLogFromResultSet(rs);
                } else {
                    return null;
                }

            }
        }

    }


    public void addAccessLog(AccessLog accessLog) throws SQLException {

        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO access_log (name, description) VALUES (?, ?);")) {


            stmt.setString(1, accessLog.getName());
            stmt.setString(2, accessLog.getDescription());

            stmt.executeUpdate();

        }

    }



    public void deleteAccessLog(int id) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM access_log WHERE id = ?;")) {

            stmt.setInt(1, id);
            stmt.executeUpdate();

        }
    }


    private AccessLog accessLogFromResultSet(ResultSet rs) throws SQLException {
        return new AccessLog(rs.getInt(1),rs.getString(2),
                rs.getString(3),  rs.getTimestamp(4));
    }


    @Override
    public void close() throws SQLException {
        this.conn.close();
    }
}
