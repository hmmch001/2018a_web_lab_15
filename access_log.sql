DROP TABLE IF EXISTS access_log;

create table access_log(
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(30),
  description VARCHAR(100),
  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);

INSERT INTO access_log (name, description) VALUES
  ('Tom', 'testing'),
  ('Bob', 'still testing'),
  ('Sarah', 'also testing');
